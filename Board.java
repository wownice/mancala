public class Board 
{
	private int rowLength; //6 or 10
	private int[] cups;
	private int player1CupIndex; //right
	private int player2CupIndex; //left
		
	/*
	 	13 12 11 10 9 8
	  0					7
	     1  2  3  4 5 6	 
	 */
	/*
	 	 21 20 19 18 17 16 15 14 13 12
	  0									11
	     1  2  3  4  5  6  7  8  9  10	 
	 */
	public Board(int length)
	{
		rowLength = length;
		player1CupIndex = rowLength + 1;
		player2CupIndex = 0;
		cups = new int[rowLength * 2 + 2];
		fillRows();
	}
	
	public Board()
	{
		rowLength = 6;
		player1CupIndex = rowLength + 1; //7
		player2CupIndex = 0;
		cups = new int[rowLength * 2 + 2]; //14
		fillRows();

	}
	
	public void fillRows()
	{
		for(int i = 1; i < cups.length; i++)
		{
			cups[i] = 4;
		}
		cups[player1CupIndex] = 0;
		cups[player2CupIndex] = 0;
	}

	public int distribute(int index)//returns last landed index
	{
		int curIndex = -1;
		
		if(!isPlayerIndex(index))
		{
			int beanCount = cups[index];
			curIndex = 0;
			cups[index] = 0;
			int counter = 0;
			index++;
			while(counter < beanCount)
			{
				curIndex = (index + counter) % cups.length;
				cups[curIndex]++;
				counter++;
			}
		}
				
		return curIndex;
	}
	
	public boolean emptyRow(int team)
	{
		if(team == 1)
		{
			for(int i = 1; i < player1CupIndex; i++)
			{
				if(!isEmptyCup(i))
				{
					return false;
				}
			}
		}
		else if(team == 2)
		{		
			for(int i = player1CupIndex + 1; i < rowLength * 2 + 2; i++)
			{
				if(!isEmptyCup(i))
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	public boolean isEmptyCup(int index)
	{
		return cups[index] == 0;
	}
	
	public void removeAndAdd(int landedIndex) //for if last piece (landedIndex) lands on an empty cup
	{
		int opposite = getOppositeIndex(landedIndex);
		cups[getPlayerIndex(getTeamFromRange(landedIndex))] += (cups[opposite] + 1);
		cups[landedIndex] = 0;
		cups[opposite] = 0;
	}
	
	public int getOppositeIndex(int ind)
	{
		return cups.length - ind;
	}
		
	
	public int getPlayerIndex(int i)
	{
		if(i == 1)
		{
			return player1CupIndex;
		}
		return player2CupIndex;
	}
	
	public int getRowLength()
	{
		return rowLength;
	}

	public int[] getCups()
	{
		return cups;
	}

	public int getValue(int index)
	{
		return cups[index];
	}
	
	public void updateBoard(int[] newBoard)
	{
		cups = newBoard;
	}
	
	public boolean isPlayerIndex(int index)
	{
		return (index == player1CupIndex || index == player2CupIndex);
	}
	
	public int getTeamFromRange(int index)
	{
		if(index >= 1 && index <= rowLength + 1)
		{
			return 1;
		}
		
		return 2;
	}
}
