import java.util.ArrayList;

import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.util.Duration;

public class UI 
{
	private Game game;
	private Board board;
	private CupPane[] panes;
	private Stage stage;
	private Label currentPlayerLabel;
	private AIPlayer ai;
	
	//animation variables
	private int counter;
	private int curIndex; 
	private boolean clickable;
	
	public UI(Stage s, Game g)
	{
		stage = s;
		game = g;
		board = game.getBoard();
		//loadStart()
		ai = new AIPlayer(2, game);
		loadBoard();
		clickable = true;
	}
	
	public UI()
	{
		
	}
	
	public void loadBoard()
	{
		VBox gameBox = new VBox(); //board on top, user stuff on bottom
		HBox rootBox = new HBox();	//player2cup, row1 + row2, player1cup
		VBox centerCups = new VBox(); //row1 + row2
		HBox player2Half = new HBox();	//row2
		HBox player1Half = new HBox(); //row1
		
		panes = new CupPane[game.getBoard().getCups().length];
		for(int i = 1; i < board.getPlayerIndex(1); i++)
		{
			panes[i] = new CupPane(i);
			player1Half.getChildren().add(panes[i]);
		}
		
		for(int i = panes.length - 1; i > board.getPlayerIndex(1); i--)
		{
			panes[i] = new CupPane(i);
			player2Half.getChildren().add(panes[i]);
		}
		
		panes[board.getPlayerIndex(2)] = new CupPane(board.getPlayerIndex(2));
		panes[board.getPlayerIndex(1)] = new CupPane(board.getPlayerIndex(1));
		panes[board.getPlayerIndex(2)].setMaxHeight(500);
		panes[board.getPlayerIndex(1)].setMaxHeight(500);
		
		centerCups.getChildren().addAll(player2Half, player1Half);
		rootBox.getChildren().addAll(panes[board.getPlayerIndex(2)],
				centerCups,panes[board.getPlayerIndex(1)]);

		currentPlayerLabel = new Label();
		updatePlayerTurnLabel();
		
		gameBox.getChildren().addAll(rootBox, currentPlayerLabel);
		Scene gameScene = new Scene(gameBox);
		stage.setScene(gameScene);
		stage.show();
	}	
	
	public void updatePlayerTurnLabel()
	{
		currentPlayerLabel.setText("Player " +  game.getCurrentPlayer() + "'s turn");
	}
	
	public void updatePlayerTurnAndAnimate()
	{
		updatePlayerTurnLabel();
		Path path = new Path();
		path.getElements().add(new MoveToAbs(currentPlayerLabel, 
				currentPlayerLabel.getLayoutX() - 10, currentPlayerLabel.getLayoutY() - 10));
        path.getElements().add(new LineToAbs(currentPlayerLabel, currentPlayerLabel.getLayoutX(), 
        		currentPlayerLabel.getLayoutY()));				
        
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.millis(100));
        pathTransition.setInterpolator(Interpolator.EASE_BOTH);
        pathTransition.setNode(currentPlayerLabel);
        pathTransition.setPath(path);
        pathTransition.play();
	}
	
	public void finishAnimation(int index)
	{
		//update self when all is done	
		panes[index].updateBeanCount(board.getCups()[index]);
		if(game.checkVictory() > 0)
		{
			currentPlayerLabel.setText(game.checkVictory() + " wins!");											
		}
		else
		{
			updatePlayerTurnAndAnimate();
			clickable = true;
			if(ai.getTeam() == game.getCurrentPlayer())
			{
				moveAndAnimate(ai.getBestOption());
			}
		}
	}
	
	public void moveAndAnimate(int index)
	{
		if(game.makeMove(index) >= 0)
		{			
			clickable = false;
			counter = 1; //start at 1 to prevent self sabotage
			curIndex = index;
			Path path = new Path();
	        PathTransition pathTransition = new PathTransition();
	        pathTransition.setDuration(Duration.millis(100));
	        pathTransition.setInterpolator(Interpolator.EASE_BOTH);
	        pathTransition.setPath(path);
			pathTransition.setNode(panes[index].getInvisPane());
			
	        pathTransition.setOnFinished(new EventHandler<ActionEvent>()
	        {
	            @Override
	            public void handle(ActionEvent event) 
	            {
	            	if(counter < panes[index].getBeanCount() + 1) //beanCount + 1 because of counter at 1)
					{					
						path.getElements().clear();
						curIndex = (index + counter) % board.getCups().length;
						
				        path.getElements().add(new MoveToAbs(panes[curIndex].getBeansPane(),
				        		panes[curIndex].getBeansPane().getLayoutX(),
				        		panes[curIndex].getBeansPane().getLayoutY()));
						path.getElements().add(new LineToAbs(panes[curIndex].getBeansPane(), 
								panes[curIndex].getBeansPane().getLayoutX(), 
								panes[curIndex].getBeansPane().getLayoutY() - 100));
						
						ImageView node = new ImageView(panes[index].getBeansPane().
								getBeansArray().get(0).getImage());
						panes[index].getInvisPane().getChildren().add(node);
						panes[curIndex].updateBeanCount(board.getCups()[curIndex]);
						if(board.getCups()[curIndex] == 0)
						{
							panes[board.getPlayerIndex(board.getTeamFromRange(curIndex))]
									.getBeansPane().add(panes[index].getBeansPane().remove());				
						}
						else
						{
							panes[curIndex].getBeansPane().add(panes[index].getBeansPane().remove());
						}
						counter++;
						pathTransition.play();									
					}
					else
					{
						panes[index].getInvisPane().getChildren().clear();
						int opposite = board.getOppositeIndex(curIndex);
						//for if a steal is successful
						if(!board.isPlayerIndex(curIndex) && board.getCups()[opposite] == 0 &&
								!panes[opposite].getBeansPane().getChildren().isEmpty())
						{
							pathTransition.setOnFinished(new EventHandler<ActionEvent>()
					        {
					            @Override
					            public void handle(ActionEvent event) 
					            {
					            	if(!panes[opposite].getBeansPane().getChildren().isEmpty())
					            	{
								        path.getElements().add(
								        		new MoveToAbs(panes[opposite].getBeansPane(), 
								        				panes[opposite].getBeansPane().getLayoutX(),
								        				panes[opposite].getBeansPane().getLayoutY()));
										path.getElements().add(
												new LineToAbs(panes[opposite].getBeansPane(),
														panes[opposite].getBeansPane().getLayoutX(),
														panes[opposite].getBeansPane().getLayoutY()
														- 100));
										
					            		ImageView node = new ImageView(
					            				panes[opposite].getBeansPane()
					            				.getBeansArray().get(0).getImage());
					            		
					            		panes[index].getInvisPane().getChildren().add(node);
										panes[curIndex].updateBeanCount(board.getCups()[curIndex]);
					            		panes[board.getPlayerIndex(board.getTeamFromRange(curIndex))].getBeansPane().
										add(panes[opposite].getBeansPane().remove());
					            		pathTransition.play();
					            	}
					            	else
					            	{
					            		panes[index].getInvisPane().getChildren().clear();
					            		//update opposite's images
										panes[opposite].updateBeanCount(board.getCups()[opposite]);
										//update playerIndex count
										panes[board.getPlayerIndex(board.getTeamFromRange(index))].
										updateBeanCount(board.getCups()[board.getPlayerIndex
										                                (board.getTeamFromRange(index))]);
										finishAnimation(index);
					            	}
					            }
					        });
							pathTransition.play();
						}
						else
						{
							panes[index].updateBeanCount(board.getCups()[index]); //update self when all is done
							finishAnimation(index);
						}
					}
	            }
	        });
	        pathTransition.play();
        }
	}
	
	public class CupPane extends Pane
	{
		private int beanCount;
		private int index;
		private Label l;
		private BeansPane bp;
		
		//animation variables
		private Pane invisPane;
		
		public CupPane(int ind)
		{
			index = ind;
			l = new Label();
			updateBeanCount(board.getCups()[index]);
			setStyle("-fx-border-color: black");
			setMinWidth(100);
			setMinHeight(200);
			setMaxWidth(100);
			setMaxHeight(200);
			bp = new BeansPane();
			invisPane = new Pane();
			getChildren().addAll(bp, l, invisPane);
		}
		
		public Pane getInvisPane()
		{
			return invisPane;
		}
		
		public int getBeanCount()
		{
			return beanCount;
		}
		
		public void updateBeanCount(int count)
		{
			beanCount = count;
			l.setText(beanCount + "");
			updateClicker();
		}
		
		public BeansPane getBeansPane()
		{
			return bp;
		}
			
		public void updateClicker()
		{
			setOnMouseClicked(new EventHandler<MouseEvent>() 
			{
				@Override
				public void handle(MouseEvent event) 
				{	
					if(clickable)
						moveAndAnimate(index);
				}													
			});
		}
	
	class BeansPane extends Pane
	{
		private ArrayList<ImageView> beansArray;
		int fourCount = 0;
		
		public BeansPane()
		{
			beansArray = new ArrayList<ImageView>(); 
			setMinWidth(100);
			setMinHeight(200);
			setMaxWidth(100);
			setMaxHeight(200);	
			createDisplay();
		}
		
		public void createDisplay()
		{
			ImageView iv;	
			fourCount = 0;
			for(int i = 0; i < beanCount; i++)
			{
				iv = new ImageView("bb.png"); // add a random value to the end for different colors
				add(iv);
			}
		}
		
		public void add(ImageView iv)
		{
			if(iv != null)
			{
				beansArray.add(iv);
				iv.setX(0);
				if(fourCount >= 4)
				{
					iv.setX(50);
					if(fourCount >= 7)
					{
						fourCount = 0;
					}
				}
				fourCount++;
				iv.setY(((beansArray.size() - 1) * 50) % 200);
				getChildren().add(iv);
			}
		}
		
		public ImageView remove() //gets first element
		{
			if(beansArray == null)
			{
				return null;
			}
			
			ImageView iv = beansArray.remove(0);
			getChildren().remove(0);
			
			fourCount = 0;
			for(int i = 0; i < getChildren().size(); i++)
			{
				getChildren().add(getChildren().remove(0));
			}
			return iv;
		}
		
		public ArrayList<ImageView> getBeansArray()
		{
			return beansArray;
		}
	}
		
	}
	
	public class MoveToAbs extends MoveTo
	{
	    public MoveToAbs(Node node) {
	        super(node.getLayoutBounds().getWidth() / 2, node.getLayoutBounds().getHeight() / 2);
	    }
	
	    public MoveToAbs(Node node, double x, double y) {
	        super(x - node.getLayoutX() + node.getLayoutBounds().getWidth() / 2, y - node.getLayoutY() + node.getLayoutBounds().getHeight() / 2);
	    }
	
	}
	
	public class LineToAbs extends LineTo 
	{
	    public LineToAbs(Node node, double x, double y) {
	        super(x - node.getLayoutX() + node.getLayoutBounds().getWidth() / 2, y - node.getLayoutY() + node.getLayoutBounds().getHeight() / 2);
	    }
	}
}
