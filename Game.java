public class Game 
{
	private Board board;
	private int currentPlayer;
	private boolean winnerExists;
	
	public Game()
	{
		board = new Board();
		currentPlayer = 1;
		winnerExists = false;
	}
	
	public int makeMove(int index) //returns last landed index
	{
		if(!winnerExists && !board.isPlayerIndex(index) && 
				((currentPlayer == 1 && index >= 1 && index <= board.getRowLength()) ||
				(currentPlayer == 2 && index >= board.getRowLength() + 2 && 
					index <= (board.getRowLength() * 2) + 1)) && board.getCups()[index] > 0)
		{
			int lastIndex = board.distribute(index);
			if(!canMoveAgain(lastIndex))
			{
				if(canTakeFromOpposite(lastIndex))
				{
					board.removeAndAdd(lastIndex);
					lastIndex = board.getOppositeIndex(lastIndex);
				}
				switchPlayer();
			}
			return lastIndex;
		}
		return -1;
	}
	
	public boolean canMoveAgain(int lastIndex)
	{
		return (lastIndex == board.getPlayerIndex(currentPlayer));
	}
	
	public boolean canTakeFromOpposite(int lastIndex)
	{
		/*
		 if index is not a playerIndex
		 lastIndex only has 1 bean
		 lastIndex is part of currentPlayer's side
		 oppositeIndex is >= 1
		 */
		return (!board.isPlayerIndex(lastIndex) &&
				board.getCups()[lastIndex] == 1 && board.getTeamFromRange(lastIndex) == currentPlayer
				&& board.getCups()[board.getOppositeIndex(lastIndex)] >= 1);
	}
	
	public void switchPlayer()
	{
		if(currentPlayer == 1)
		{
			currentPlayer = 2;
		}
		else
		{
			currentPlayer = 1;
		}
	}

	public int getCurrentPlayer()
	{
		return currentPlayer;
	}
	
	public int checkVictory() //-1 means no winner
	{
		if(board.emptyRow(1) || board.emptyRow(2))
		{
			winnerExists = true;
			if(board.getCups()[board.getPlayerIndex(1)] > board.getCups()[board.getPlayerIndex(2)])
			{
				return 1;
			}
			
			return 2;
		}
		return -1;
	}
	
	public Board getBoard()
	{
		return board;
	}
}
