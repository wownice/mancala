public class BinaryNode
{
	private int value;
	private int index;
	private BinaryNode left;
	private BinaryNode right;
	
	public BinaryNode(){}
	
	public BinaryNode(int ind, int val)
	{
		index = ind;
		value = val;
	}
	
	public void insert(BinaryNode root, int ind, int val)
	{
		if(index == 0 && value == 0)
		{
			value = val;
			index = ind;
		}
		else
		{
			if(val > root.value)
			{
	            if(root.right == null)
	            	root.right = new BinaryNode(ind, val);
	            else
	                insert(root.right, ind, val);
			}
	        else
	        {
	            if(root.left == null)
	                root.left = new BinaryNode(ind, val);
	            else
	                insert(root.left, ind, val);
	        }
		}
	}
	
	public BinaryNode getMax(BinaryNode node)
	{
		if(node == null)
			return null;
		else if(node.right == null)
			return node;
		return getMax(node.right);
	}
	
	public BinaryNode getLeft()
	{
		return left;
	}
	
	public BinaryNode getRight()
	{
		return right;
	}
	
	public int getValue()
	{
		return value;
	}
	
	public int getIndex()
	{
		return index;
	}
}