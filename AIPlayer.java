public class AIPlayer 
{
	private int team;
	private Game game;
	private Board board;
	
	public AIPlayer(int cteam, Game g)
	{
		team = cteam;
		game = g;
		board = game.getBoard();
	}
	
	public int getBestOption()
	{		
		BinaryNode moves = new BinaryNode();
		if(team == 1)
		{
			for(int i = 1; i < board.getRowLength(); i++)
			{
				moves.insert(moves, i, analyzeMove(i));
			}
			
			return moves.getMax(moves).getIndex();
		}
		
		for(int i = board.getRowLength() + 2; i < board.getRowLength() * 2 + 2; i++)
		{
			moves.insert(moves, i, analyzeMove(i));
		}
		
		return moves.getMax(moves).getIndex();
	}
	
	public int analyzeMove(int index)
	{
		int[] cups = board.getCups();
		
		if(cups[index] == 0) //empty space
		{
			//System.out.println("index: " + index + "-9999 for being empty");
			return -9999;
		}
		
		int score = 0;
		int initialPlayer = game.getCurrentPlayer();
		int[] tempBoard = newTempBoard();
		int oldOppositePlayerScore = cups[board.getPlayerIndex(getOppositePlayerTeam())];
		int lastIndex = game.makeMove(index);
		//System.out.print("curIndex : " + index + " : ");
		
		int currentOppositePlayerScore = cups[board.getPlayerIndex(getOppositePlayerTeam())];
		
		if(lastIndex == board.getPlayerIndex(team)) //if can move again
		{
			//System.out.print( "   added 50 for moving again, ");
			score += 50;
		}
		
		if(currentOppositePlayerScore > oldOppositePlayerScore)
		{	//if giving to opposite player
			//System.out.print("   deducted " + (currentOppositePlayerScore - oldOppositePlayerScore) +
			//		" for handing to opp, ");
			score -= 25 * (currentOppositePlayerScore - oldOppositePlayerScore);	
		}
		
		//if steal is successful
		//Conditions: not player index, last actual landed index and lastIndex are 0
		//lastIndex will actually be the opposite of actual landed index at this point
		//PlayerIndex = Old Player Index + Opposite + 1
		if(!board.isPlayerIndex(lastIndex) && cups[lastIndex] == 0 && 
				cups[board.getOppositeIndex(lastIndex)] == 0 &&
				cups[board.getPlayerIndex(team)] == tempBoard[board.getPlayerIndex(team)] + 
					tempBoard[lastIndex] + 1)
		{
			//System.out.println("   added " + (5 * tempBoard[lastIndex]) + " for stealing from opp, ");
			score += 5 * (tempBoard[lastIndex]);
		}
		
		//deduct points if a move leads to a good move on opp such as giving them a move to go again
		if(board.getTeamFromRange(lastIndex) != team)
		{
			if(game.getCurrentPlayer() == initialPlayer)
			{
				game.switchPlayer();
			}
			
			int[] tempBoard2;
			int movedIndex;
			while(board.getTeamFromRange(lastIndex) != team)
			{
				tempBoard2 = newTempBoard();
				movedIndex = game.makeMove(lastIndex);
				lastIndex--;
				
				if(movedIndex > 0)
				{
					if(movedIndex == board.getPlayerIndex(board.getTeamFromRange(movedIndex)))
					{
						//System.out.print("    removed 25 for giving extra move opportunity, ");
						score -= 25;
					}
					if(!board.isPlayerIndex(movedIndex) && cups[movedIndex] == 0 && 
							cups[board.getOppositeIndex(movedIndex)] == 0 &&
							cups[board.getPlayerIndex(team)] == tempBoard[board.getPlayerIndex(team)] + 
								tempBoard[movedIndex] + 1)
					{
						//System.out.println("   removed " + 
						//(5 * tempBoard[lastIndex]) + " for giving opp steal opportunity, ");
						score -= 5 * (tempBoard[movedIndex]);
					}
				}
				
				game.switchPlayer();
				board.updateBoard(tempBoard2);
			}
		}
		
		if(game.getCurrentPlayer() != initialPlayer)
		{
			game.switchPlayer();
		}
		
		board.updateBoard(tempBoard);
		//get score
		//System.out.println("   score: " +score);
		return score;
	}
	
	public int[] newTempBoard()
	{
		int[] tempBoard = new int[board.getCups().length];
		for(int i = 0; i < board.getCups().length; i++)
		{
			tempBoard[i] = board.getCups()[i];
		}
		
		return tempBoard;
	}
	
	public int getTeam()
	{
		return team;
	}
	
	public int getOppositePlayerTeam()
	{
		if(team == 2)
		{
			return 1;
		}
		
		return 2;
	}
}
